use db_QuanlyQuanCafe

select * from tblNhanvien ,tblVitri, tblVitriNhanvien
where PK_iVitriID=FK_iVitriID and FK_iNhanvienID=PK_iNhanvienID

select PK_iVitriID,sTenVitri from tblVitri
select * from tblVitriNhanvien

alter procedure sp_themNhanVien(@ten nvarchar(100),@ngaysinh date, @gioitinh bit,
@sodienthoai char(10),@diachi nvarchar(500),@tendangnhap nvarchar(100),@matkhau varchar(100),
@vitriid int,@ngaybatdau date,@ngayketthuc date)
as
begin
	insert into tblNhanvien(sTenNhanvien,tNgaysinh,bGioitinh,sSoDienthoai,sDiachi,sTenDangnhap,sMatkhau)
	values (@ten,@ngaysinh,@gioitinh,@sodienthoai,@diachi,@tendangnhap,@matkhau)

	insert into tblVitriNhanvien(FK_iNhanvienID,FK_iVitriID,tNgayBatdau,tNgayKetthuc)
	values (IDENT_CURRENT('tblNhanvien'),@vitriid,@ngaybatdau,@ngayketthuc)
end