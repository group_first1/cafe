﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Layout.Master" AutoEventWireup="true" CodeBehind="DanhSachNhanVien.aspx.cs" Inherits="btl.View.DanhSachNhanVien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Danh sách nhân viên | Hệ thống quản lý Góc Coffee</title>
        <!--dynamic table-->
    <link href="../assets/template/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="../assets/template/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="../assets/template/assets/data-tables/DT_bootstrap.css" />

      <style type="text/css">
        table.table th, table.table td{
            vertical-align: middle;
        }
        table.table td:first-child{
            width: 75px;
        }
        .flex, .flex>div{
            display: flex;
            align-items: center;
        }
        td.form-group{
            width: 175px;
        }
        .flex label{
            margin: 0;
        }
        .flex .form-group{
            margin: 0;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                    <li class="breadcrumb-item">Quản lý nhân viên</li>
                    <li class="breadcrumb-item active" aria-current="page">Danh sách nhân viên</li>
                </ol>
            </nav>
            <!--breadcrumbs end -->
        </div>
    </div>
    <!--state overview start-->
    
    <div class="row state-overview">
        <div class="col-md-12">
            <section class="card">
                <div class="card-header">
                    <h3 class="text-center">Danh sách nhân viên</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="dynamic-table">
                        <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Nhân viên</th>
                                <th class="text-center">Ngày sinh</th>
                                <th class="text-center">Số điện thoại</th>
                                <th class="text-center">Tài khoản</th>
                                <th class="text-center">Vị trí</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:ListView ID="ListNhanVien" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center"><%# SoThuTu() %></td>
                                        <td><%#:Eval("sTenNhanvien") %></td>
                                        <td class="text-center"><%#:Eval("tNgaysinh","{0:dd/MM/yyyy}") %></td>
                                        <td class="text-center"><%#:Eval("sSoDienthoai") %></td>
                                        <td class="text-center"><%#:Eval("sTenDangnhap") %></td>
                                        <td><%#:Eval("sTenVitri") %></td>
                                        <td class="text-center">
                                            <button class="btn btn-sm btn-default mr-1" idNV='<%#:Eval("PK_iNhanvienID") %>'>
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-sm btn-warning ml-1 mr-1" idNV='<%#:Eval("PK_iNhanvienID") %>'>
                                                <i class="fa fa-lock"></i>
                                            </button>
                                            <button class="btn btn-sm btn-info ml-1" idNV='<%#:Eval("PK_iNhanvienID") %>'>
                                                <i class="fa fa-key"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
    <!--state overview end-->

    <script type="text/javascript" language="javascript" src="../assets/template/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../assets/template/assets/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
    <script src="../assets/template/js/dynamic_table_init.js"></script>
   
</asp:Content>
