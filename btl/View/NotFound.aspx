﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Layout.Master" AutoEventWireup="true" CodeBehind="NotFound.aspx.cs" Inherits="btl.View.NotFound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <title>page not found</title>
    <style>
        div#wrap-notfound {
            background-color: white;
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            font-family: sans-serif;
            padding-top: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="wrap-notfound">
         <h1>Page Not Found</h1>
        <img src="notfound.gif" style="width:600px;" />
    </div>
   
</asp:Content>
