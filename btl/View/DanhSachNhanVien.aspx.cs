﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using btl.Controller;
using btl.Model;
namespace btl.View
{
    public partial class DanhSachNhanVien : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = NhanVien.Instance.GetListNhanVien();
            ListNhanVien.DataSource = dt;
            ListNhanVien.DataBind();
        }
        int sothutu = 0;
        protected String SoThuTu()
        {
            sothutu++;
            return sothutu.ToString();
        }
    }
}