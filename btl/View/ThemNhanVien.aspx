﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/Layout.Master" AutoEventWireup="true" CodeBehind="ThemNhanVien.aspx.cs" Inherits="btl.View.ThemNhanVien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Thêm nhân viên | Hệ thống quản lý Góc Coffee</title>
     <style type="text/css">
        table.table th, table.table td{
            vertical-align: middle;
        }
        table.table td:first-child, table.table td:last-child{
            width: 75px;
        }
        .flex, .flex>div{
            display: flex;
            align-items: center;
        }
        td.form-group{
            width: 175px;
        }
        .flex label{
            margin: 0;
        }
        .flex .form-group{
            margin: 0;
            width: 100%;
        }
        p.error-item {
            padding: 10px;
            color: white;
            background: #f902029e;
            font-family:sans-serif;
            border-radius: 5px;
            margin-bottom: 10px;
            /* width: fit-content; */
            animation-name: appear;
            animation-duration: 0.5s;
        }
        @keyframes appear {
          from {
              transform:translateX(100%)
          }
          to {
             transform:translateX(0%)
          }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-lg-12">
            <!--breadcrumbs start -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                    <li class="breadcrumb-item">Quản lý nhân viên</li>
                    <li class="breadcrumb-item active" aria-current="page">Thêm nhân viên</li>
                </ol>
            </nav>
            <!--breadcrumbs end -->
        </div>
    </div>
    <!--state overview start-->
    <div class="row state-overview">
        <div class="col-md-12">
            <section class="card">
                <div class="card-header">
                    <h3 class="text-center">Thêm nhân viên</h3>
                </div>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="col-md-4 mb-3">
                            <label for="tennhanvien">Tên nhân viên (<span class="text-danger">*</span>)</label>
                            <input type="text" id="tennhanvien" placeholder="VD: Bùi Văn Hùng" class="form-control">
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="tennhanvien">Giới tính (<span class="text-danger">*</span>)</label>
                            <div class="flex">
                                <input type="radio" id="male" value="0" name="gender"> <label for="male"> &nbsp; Nam</label>
                                &ensp;&ensp;&ensp;
                                &ensp;&ensp;&ensp;
                                <input type="radio" id="female" value="1" name="gender"> <label for="female"> &nbsp; Nữ</label>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="ngaysinh">Ngày sinh (<span class="text-danger">*</span>)</label>
                            <input type="date" id="ngaysinh" class="form-control">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="sodienthoai">Số điện thoại (<span class="text-danger">*</span>)</label>
                            <input type="text" placeholder="VD: 0329 222 617" id="sodienthoai" class="form-control">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="tennhanvien">Địa chỉ</label>
                            <textarea id="diachi" placeholder="VD: 296 Minh Khai" class="form-control" rows="1"></textarea>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="tennhanvien">Tên đăng nhập (<span class="text-danger">*</span>)</label>
                            <input type="text" id="tendangnhap" class="form-control" placeholder="VD: vanhung1402">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="tennhanvien">Mật khẩu (<span class="text-danger">*</span>)</label>
                            <input type="password" id="matkhau" class="form-control">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="tennhanvien">Xác nhận mật khẩu (<span class="text-danger">*</span>)</label>
                            <input type="password" id="makhau2" class="form-control">
                        </div>
                    </div>
                   <div class="row">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <%--<th class="text-center">STT</th>--%>
                                                <th class="text-center">Vị trí (<span class="text-danger">*</span>)</th>
                                                <th class="text-center">Ngày bắt đầu (<span class="text-danger">*</span>)</th>
                                                <th class="text-center">Ngày kết thúc (<span class="text-danger">*</span>)</th>
                                                <%--<th class="text-center">
                                                    <button class="btn btn-sm btn-primary">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </th>--%>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <%--<td class="text-center">1</td>--%>
                                                <td class="form-group">
                                                    <select class="form-control form-control-lg" id="vitri">
                                                        <option selected="" disabled="">--- Chọn vị trí ---</option>
                                                    </select>
                                                </td>
                                                <td class="form-group">
                                                    <input type="date" id="ngaybatdau" class="form-control">
                                                </td>
                                                <td class="form-group">
                                                    <input type="date" id="ngayketthuc" class="form-control">
                                                </td>
                                                <%--<td class="text-center">
                                                    <button class="btn btn-sm btn-warning">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                </td>--%>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                    <div class="form-group text-center mt-3">
                        <button class="btn btn-success" type="submit" onclick="btnThemNhanVienClick();">
                            <i class="fa fa-check mr-2"></i>Thêm nhân viên
                        </button>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div id="Notifications" style="position:fixed;width:280px;z-index:1000;top: 70px;right: 0;display: none;"></div>
    <!--state overview end-->
    <script type="text/javascript">
         window.addEventListener('load', (event) => {
            loadVitri();
        });
        function loadVitri() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                   // alert(this.responseText);
                    hienThiViTri(this.responseText);
                }
            };
            xhttp.open("GET", "/Controller/ViTriController.aspx?process=getListViTri", true);
            xhttp.send();
        }

        function hienThiViTri(sData) {
            var jsonData = JSON.parse(sData);
            if (jsonData.length > 0)
            {

                for (let i = 0; i < jsonData.length; i++)
                {
                    var option = document.createElement("option");
                    option.value =  jsonData[i].PK_iVitriID; 
                    var node = document.createTextNode(jsonData[i].sTenVitri);
                    option.appendChild(node);

                    viTri.appendChild(option);
                }
           
            }

        }

        var tenNhaVien = document.getElementById('tennhanvien');
        var ngaySinh = document.getElementById('ngaysinh');
        var soDienThoai = document.getElementById('sodienthoai');
        var tenDangNhap = document.getElementById('tendangnhap');
        var matKhau = document.getElementById('matkhau');
        var matKhau2 = document.getElementById('makhau2');
        var viTri = document.getElementById('vitri');
        var ngayBatDau = document.getElementById('ngaybatdau');
        var ngayKetThuc = document.getElementById('ngayketthuc');
        var notifications = document.getElementById('Notifications'); 
        var nam = document.getElementById('male'); 
        var nu = document.getElementById('female'); 
        var diachi = document.getElementById('diachi'); 
        let arrErr = [];//để lưu nhứng error đã xuất hiện, tránh hiện cùng 1 err nhiều lần
        let isError = 0;

        function btnThemNhanVienClick() {
            if (tenNhaVien.value.length <= 0) 
                showError("Tên nhân viên không được để trống", 1);
            if (nam.checked == false && nu.checked == false)
                showError("Bạn chưa chọn giới tính", 111);
            if (ngaySinh.value.length <= 0) 
                showError("Ngày sinh nhân viên không được để trống", 2);
            if (soDienThoai.value.length <= 0)
                showError("Số điện thoại không được để trống",3);
            if (tenDangNhap.value.length <= 0)
                showError("Tên đăng nhập không được để trống",4);
            if (matKhau.value.length <= 0)
                showError("Mật khẩu không được để trống",5);
            if (matKhau2.value.length <= 0)
                showError("Nhập lại mật khẩu không được để trống",6);
            if (viTri.selectedIndex == 0)
                showError("Bạn chưa chọn vị trí",7);
            if (ngayBatDau.value.length <= 0)
                showError("Ngày bắt đầu không được để trống",8);
            if (ngayKetThuc.value.length <= 0)
                showError("Ngày kết thúc không được để trống", 9);
            if (matKhau.value.length != 0 && matKhau2.value.length != 0 && matKhau.value != matKhau2.value)
                showError("Mật khẩu không giống xác nhận mật khẩu", 10);

            var re = /^0+[0-9]{9}$/;
            if (!re.exec(soDienThoai.value) && soDienThoai.value.length != 0)
                showError("Số điện thoại không đúng định dạng", 11);

            let date1 = new Date(ngayBatDau.value); 
            let date2 = new Date(ngayKetThuc.value);
            if (ngayBatDau.value.length > 0 && ngayKetThuc.value.length > 0 && date1 >= date2)
                 showError("Ngày kết thúc phải lớn hơn ngày bắt đầu", 12);

            if (isError == 0)
                ThemNhanVien();
        }
        
        function showError(message, type) {
            if (arrErr.indexOf(type)!=-1)//nếu err có type này đang show thì ko show nữa
                return;
            arrErr.push(type)

            var para = document.createElement("p");
            para.classList.add("error-item");
            var node = document.createTextNode(message);
            para.appendChild(node);

            notifications.insertBefore(para, notifications.firstChild);
            notifications.style.display = 'block';
            isError++;

            setTimeout(function ad() {
                isError--;
                arrErr.splice(arrErr.indexOf(type), 1);
                para.remove();
                if (isError == 0)
                    notifications.style.display = 'none';
            },3000)
        }

        function ThemNhanVien() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.responseText == 2)
                        showError("Tên đăng nhập đã tồn tại", 222);
                    else if (this.responseText == -1)
                        showError("Thêm nhân viên thất bại", 222);

                    else {
                        showError("Thêm nhân viên thành công", 222);
                        tenNhaVien.value = "";
                        ngaySinh.value = "";
                        nam.checked = false;
                        nu.checked = false;
                        soDienThoai.value = "";
                        tenDangNhap.value = "";
                        matKhau.value = "";
                        matKhau2.value = "";
                        ngayBatDau.value = "";
                        ngayKetThuc.value = "";
                        diachi.value = "";
                    }
                }
            };
            let gt = nam.checked?0:1;
            let link = "/Controller/NhanVienController.aspx?process=addNV&ten=" + tenNhaVien.value + "&ns=" + ngaySinh.value + "&gt=" + gt +
                "&sdt=" + soDienThoai.value + "&tdn=" + tenDangNhap.value + "&mk=" + matKhau.value + "&vt=" + viTri.value + "&nbd=" +
                ngayBatDau.value + "&nkt=" + ngayKetThuc.value + "&dc=" + diachi.value;
            xhttp.open("POST",link, true);
            xhttp.send();
        }
    </script>
</asp:Content>
