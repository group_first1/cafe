﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using btl.Model;

namespace btl.Controller
{
    public partial class ViTriController : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["process"] == "getListViTri")
                {
                    DataTable dtViTri = Vitri.Instance.GetListViTri();
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(ConvertDatatable.Instance.DataTableToJsonObj(dtViTri));
                    Response.End();
                }
            }
        }
    }
}