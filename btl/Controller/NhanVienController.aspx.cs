﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using btl.Model;
using System.Data;

namespace btl.Controller
{
    public partial class NhanVienController : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["process"] == "addNV")
                {
                    bool gt = (int.Parse(Request.QueryString["gt"]) == 1 )? true : false;
                    ThemNhanVien(Request.QueryString["ten"], Request.QueryString["ns"],gt,
                        Request.QueryString["sdt"], Request.QueryString["dc"], Request.QueryString["tdn"],
                        Request.QueryString["mk"], Request.QueryString["vt"], Request.QueryString["nbd"], Request.QueryString["nkt"]);
                    
                }
            }
        }
        private void ThemNhanVien(string ten, string ngaysinh, bool gioitinh, string sodienthoai, string diachi,
            string tendangnhap, string matkhau,string idvitri,string ngaybatdau,string ngayketthuc)
        {
            Response.Clear();
            int isSucess = NhanVien.Instance.AddNhanVien(ten, ngaysinh, gioitinh, sodienthoai, diachi, tendangnhap, matkhau,idvitri,ngaybatdau,ngayketthuc);        
            Response.Write(isSucess.ToString());
            Response.End();
        }
       
    }
}