﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace btl.Model
{
    public class NhanVien
    {
        private static NhanVien instance;
        string sCnnStr = ConfigurationManager.ConnectionStrings["myCnnStr"] + "";

        public static NhanVien Instance
        {
            get
            {
                if (instance == null)
                    instance = new NhanVien();
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public int AddNhanVien(string ten ,string ngaysinh , bool gioitinh , string sodienthoai,string diachi,
            string tendangnhap ,string matkhau,string idvitri,string ngaybatdau,string ngayketthuc)
        {
            using (SqlConnection cn = new SqlConnection(sCnnStr))
            {
                using (SqlCommand cm = new SqlCommand())
                {
                    cm.Connection = cn;

                    cm.CommandType = CommandType.Text;
                    cm.CommandText = "select * from tblNhanvien where sTenDangnhap=N'"+tendangnhap+"'";
                    using (SqlDataAdapter da = new SqlDataAdapter(cm))
                    {
                        DataSet dsNV= new DataSet();
                        da.Fill(dsNV);
                        if (dsNV.Tables[0].Rows.Count > 0)
                        {
                            return 2;
                        }
                    }

                    cm.CommandType = CommandType.StoredProcedure;
                    cm.CommandText = "sp_themNhanVien";
                    cm.Parameters.AddWithValue("@ten",ten);
                    string[]  arr= ngaysinh.Split('-');//new DateTime(int.Parse(arr[0]),int.Parse(arr[1]),int.Parse(arr[2]))
                    cm.Parameters.AddWithValue("@ngaysinh",ngaysinh);
                    cm.Parameters.AddWithValue("@gioitinh", gioitinh==true?1:0);
                    cm.Parameters.AddWithValue("@sodienthoai", sodienthoai);
                    cm.Parameters.AddWithValue("@diachi", diachi);
                    cm.Parameters.AddWithValue("@tendangnhap", tendangnhap);
                    cm.Parameters.AddWithValue("@matkhau",BuildPassword( matkhau));
                    cm.Parameters.AddWithValue("@vitriid", idvitri);
                    cm.Parameters.AddWithValue("@ngaybatdau", ngaybatdau);
                    cm.Parameters.AddWithValue("@ngayketthuc", ngayketthuc);
                    cn.Open();
                    try
                    {
                        cm.ExecuteNonQuery();
                    }
                    catch
                    {
                        return -1;
                    }
                    cn.Close();
                }
            }
            return 1;
        }
        protected string BuildPassword(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            System.Text.StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));

            }
            return sBuilder.ToString();
        }

        public DataTable GetListNhanVien()
        {
            DataTable tb = new DataTable();
            using (SqlConnection cn = new SqlConnection(sCnnStr))
            {
                using (SqlCommand cm = new SqlCommand())
                {
                    cm.Connection = cn;
                    cm.CommandType = CommandType.Text;
                    cm.CommandText = @"select * from tblNhanvien ,tblVitri, tblVitriNhanvien
                                    where PK_iVitriID = FK_iVitriID and FK_iNhanvienID = PK_iNhanvienID";
                    using (SqlDataAdapter da = new SqlDataAdapter(cm))
                    {
                        DataSet dsNV = new DataSet();
                        da.Fill(dsNV);
                        if (dsNV.Tables[0].Rows.Count > 0)
                        {
                            tb = dsNV.Tables[0];
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            return tb;
        }
    }
}