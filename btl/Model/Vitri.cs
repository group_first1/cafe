﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace btl.Model
{
    public class Vitri
    {
        private static Vitri instance;
        string sCnnStr = ConfigurationManager.ConnectionStrings["myCnnStr"] + "";

        public static Vitri Instance
        {
            get
            {
                if (instance == null)
                    instance = new Vitri();
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        public DataTable GetListViTri()
        {
            DataTable tb = new DataTable();
            using (SqlConnection cn = new SqlConnection(sCnnStr))
            {
                using (SqlCommand cm = new SqlCommand())
                {
                    cm.Connection = cn;
                    cm.CommandType = CommandType.Text;
                    cm.CommandText = "select * from tblVitri";
                    using (SqlDataAdapter da = new SqlDataAdapter(cm))
                    {
                        DataSet dsVitri = new DataSet();
                        da.Fill(dsVitri);
                        if (dsVitri.Tables[0].Rows.Count > 0)
                        {
                            tb = dsVitri.Tables[0];
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            return tb;
        }
    }
}