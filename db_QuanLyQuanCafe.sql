create database db_QuanlyQuanCafe
use db_QuanlyQuanCafe


create table tblChucnang
(	
	PK_iChucnangID int IDENTITY(1,1) primary key,
	sTenChucnang nvarchar(100) not null,
	sGhichu nvarchar(500)
)

create table tblVitri
(
	PK_iVitriID int IDENTITY(1,1) primary key,
	sTenVitri nvarchar(100) not null,
)

create table tblPhanquyen
(
	FK_iVitriID	int,
	FK_iChucnangID int,
	CONSTRAINT PK_tblPhanquyen primary key ( FK_iVitriID,FK_iChucnangID),
	CONSTRAINT FK_tblPhanquyen_tblVitri FOREIGN KEY (FK_iVitriID) REFERENCES tblVitri (PK_iVitriID),
	CONSTRAINT FK_tblPhanquyen_tblChucnang FOREIGN KEY (FK_iChucnangID) REFERENCES tblChucnang (PK_iChucnangID),
)

create table tblNhanvien
(
	PK_iNhanvienID int IDENTITY(1,1) primary key,
	sTenNhanvien nvarchar(100) not null,
	tNgaysinh date not null,
	bGioitinh bit not null,
	sSoDienthoai char(10) not null,
	sDiachi nvarchar(500) not null,
	sTenDangnhap nvarchar(100) not null,
	sMatkhau varchar(100) not null
)

create table tblVitriNhanvien
( 
	FK_iNhanvienID int ,
	FK_iVitriID int ,
	tNgayBatdau date not null, 
	tNgayKetthuc date,
	CONSTRAINT PK_tblVitriNhanvien primary key (FK_iNhanvienID,FK_iVitriID),
	CONSTRAINT FK_tblVitriNhanvien_tblNhanvien FOREIGN KEY (FK_iNhanvienID) REFERENCES tblNhanvien (PK_iNhanvienID),
	CONSTRAINT FK_tblVitriNhanvien_tblVitri FOREIGN KEY (FK_iVitriID) REFERENCES tblVitri (PK_iVitriID)
)

create table tblNhacungcap
(
	PK_iNhacungcapID int identity(1,1) primary key,
	sTenNhacungcap nvarchar(100) not null,
	sSodienthoai char(10) not null,
	sDiaChi nvarchar(500) not null,
	sEmail nvarchar(100) not null
)

create table tblPhieunhap
(
	PK_iPhieunhapID int identity(1,1) primary key,
	FK_iNhanvienID int,
	FK_iNhacungcapID int,
	tNgaylap datetime not null,
	fTongtien float,
	CONSTRAINT FK_tblPhieunhap_tblNhanvien FOREIGN KEY (FK_iNhanvienID) REFERENCES tblNhanvien (PK_iNhanvienID),
	CONSTRAINT FK_tblPhieunhap_tblNhacungcap FOREIGN KEY (FK_iNhacungcapID) REFERENCES tblNhacungcap (PK_iNhacungcapID)
)

create table tblNguyenlieu
(
	PK_iNguyenlieuID int identity(1,1) primary key,
	sTenNguyenlieu nvarchar(100) not null ,
	fSoluong float not null,
	sDonvitinh nvarchar(100) not null
)

create table tblChitietPhieunhap
(
	FK_iPhieunhapID int,
	FK_iNguyenlieuID int,
	fSoluong float not null,
	fDongia float not null,
	CONSTRAINT PK_tblChitietPhieunhap primary key (FK_iPhieunhapID,FK_iNguyenlieuID),
	CONSTRAINT FK_tblChitietPhieunhap_tblNguyenlieu FOREIGN KEY (FK_iNguyenlieuID) REFERENCES tblNguyenlieu (PK_iNguyenlieuID),
	CONSTRAINT FK_tblChitietPhieunhap_tblPhieunhap FOREIGN KEY (FK_iPhieunhapID) REFERENCES tblPhieunhap (PK_iPhieunhapID)
)



create table tblLoaiDouong
(	
	PK_iLoaiDouongID int identity(1,1) primary key,
	sTenLoaiDouong nvarchar(100) not null
)

create table tblDouong 
(
	PK_iDouongID int identity(1,1) primary key,
	FK_iLoaiDouongID int ,
	sTenDouong nvarchar(100) not null,
	fDongia float not null,
	bTrangthai bit not null,
	CONSTRAINT FK_tblDouong_tblLoaiDouong FOREIGN KEY (FK_iLoaiDouongID) REFERENCES tblLoaiDouong (PK_iLoaiDouongID)
)

create table tblCongthuc
(		
	FK_iDouongID int,
	FK_iNguyenlieuID int,
	fSoluong float not null,
	sGhichu nvarchar(500),
	CONSTRAINT PK_tblCongThuc primary key (FK_iDouongID,FK_iNguyenlieuID),
	CONSTRAINT FK_tblCongThuc_tblDouong FOREIGN KEY (FK_iDouongID) REFERENCES tblDouong (PK_iDouongID),
	CONSTRAINT FK_tblCongThuc_tblNguyenlieu FOREIGN KEY (FK_iNguyenlieuID) REFERENCES tblNguyenlieu (PK_iNguyenlieuID)
)

create table tblBan
(
	PK_iBanID int identity(1,1) primary key,
	sTenBan nvarchar(100) not null
)

create table tblHoadon
(
	PK_iHoadonID int identity primary key,
	FK_iNhanvienID int ,
	FK_iBanID int,
	tNgaylap datetime not null,
	fTongtien float,
	CONSTRAINT FK_tblHoadon_tblNhanvien FOREIGN KEY (FK_iNhanvienID) REFERENCES tblNhanvien (PK_iNhanvienID),
	CONSTRAINT FK_tblHoadon_tblBan FOREIGN KEY (FK_iBanID) REFERENCES tblBan (PK_iBanID)
)

create table tblChitietHoadon
(
	FK_iHoadonID int,
	FK_iDouongID int,
	iSoluong int not null,
	fDongia float not null,
	CONSTRAINT PK_tblChitietHoadon primary key (FK_iHoadonID,FK_iDouongID),
	CONSTRAINT FK_tblChitietHoadon_tblHoadon FOREIGN KEY (FK_iHoadonID) REFERENCES tblHoadon (PK_iHoadonID),
	CONSTRAINT FK_tblChitietHoadon_tblDouong FOREIGN KEY (FK_iDouongID) REFERENCES tblDouong (PK_iDouongID)
)